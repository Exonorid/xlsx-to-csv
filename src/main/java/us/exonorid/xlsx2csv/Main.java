package us.exonorid.xlsx2csv;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main {
    public static void main(String[] args) throws IOException {
        if(args.length < 2) {
            System.err.println("Usage: xlsx2csv <in-file> <out-folder>");
            System.exit(1);
        }

        String sheet_path = null;
        String strings_path = null;
        String sheet_name = null;

        try {
            Path extract_target = Files.createTempDirectory("xlsx2csv");
            System.out.println("Extracting files to " + extract_target.toString());

            byte[] buffer = new byte[4096];
            ZipInputStream zis = new ZipInputStream(new FileInputStream(new File(args[0])));
            ZipEntry entry = zis.getNextEntry();

            while(entry != null) {
                File file = new File(extract_target.toString(), entry.getName());
                if(entry.isDirectory()) {
                    if(!file.isDirectory() && !file.mkdirs()) {
                        System.err.println("Could not create directory " + file.getAbsolutePath());
                    }
                } else {
                    File parent = file.getParentFile();
                    if(!parent.isDirectory() && !parent.mkdirs()) {
                        System.err.println("Could not create directory " + file.getAbsolutePath());
                    }

                    FileOutputStream out_stream = new FileOutputStream(file);
                    int len;
                    while((len = zis.read(buffer)) > 0) {
                        out_stream.write(buffer, 0, len);
                    }
                    out_stream.close();

                    if(entry.getName().matches("xl/worksheets/sheet[1-9]+\\.xml")) {
                        sheet_path = file.getAbsolutePath();
                        sheet_name = entry.getName();
                    } else if(entry.getName().matches("xl/sharedStrings\\.xml")) {
                        strings_path = file.getAbsolutePath();
                    }

                    System.out.println("File: " + entry.getName());
                }
                entry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        if(sheet_path == null) {
            System.err.println("The spreadsheet file provided does not appear to have any sheets");
            System.exit(1);
        }

        System.out.println("Reading data from " + sheet_name);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        File out_dir = new File(args[1]);
        if(!out_dir.isDirectory() && !out_dir.mkdirs()) {
            System.err.println("Could not create output directory " + out_dir.getCanonicalPath());
            System.exit(1);
        }
        DecimalFormat number_format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        number_format.setMaximumFractionDigits(340);
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document sheet_doc = db.parse(new File(sheet_path));
            Document strings_doc = null;

            sheet_doc.getDocumentElement().normalize();

            //Parse the shared strings, if there are any
            String[] strings = null;
            if(strings_path != null) {
                strings_doc = db.parse(new File(strings_path));
                strings_doc.getDocumentElement().normalize();
                int count = Integer.parseInt(strings_doc.getDocumentElement().getAttribute("count"));
                strings = new String[count];

                NodeList list = strings_doc.getElementsByTagName("si");
                for(int i = 0; i < count; i++) {
                    Element e = (Element) list.item(i);
                    strings[i] = e.getFirstChild().getTextContent();
                }
            }

            Element sheet_data = (Element) sheet_doc.getElementsByTagName("sheetData").item(0);
            NodeList rows = sheet_data.getElementsByTagName("row");
            int num_rows = rows.getLength();

            //Make a CSV row from the header, since it's reused across all the files
            String header_str = "";
            {
                Element header_row = (Element) rows.item(0);
                NodeList header_cells = header_row.getElementsByTagName("c");
                for(int x = 0; x < header_cells.getLength(); x++) {
                    Element header = (Element) header_cells.item(x);

                    String value = null;
                    if (header.hasAttribute("t")) {
                        String type = header.getAttributeNode("t").getValue();
                        switch (type) {
                            case "s":
                                value = strings[Integer.parseInt(header.getFirstChild().getTextContent())];
                                break;
                            case "n":
                                value = header.getFirstChild().getTextContent();
                                break;
                            case "inlineStr":
                                Element is = (Element) header.getFirstChild();
                                String contents = ((Element) is.getFirstChild()).getTextContent();
                                value = contents;
                                break;
                            default:
                                System.err.println("Unknown cell type " + type);
                                System.exit(1);
                        }
                    } else {
                        value = number_format.format(Double.parseDouble(header.getFirstChild().getTextContent()));
                    }
                    header_str += value + ",";
                }
                header_str = header_str.substring(0, header_str.length() - 1) + "\n";
            }

            //Make the output CSV file
            int file_index = 0;
            int num_written = 0;
            FileOutputStream csv_ostream = new FileOutputStream(new File(args[1] + "/" + file_index + ".csv"));

            //Write the header
            csv_ostream.write(header_str.getBytes(StandardCharsets.UTF_8));

            //Write the rest of the data
            for(int y = 0; y < num_rows - 1; y++) {
                Element row = (Element) rows.item(y + 1);
                NodeList columns = row.getElementsByTagName("c");
                int num_cols = columns.getLength();

                String row_contents = "";
                for(int x = 0; x < num_cols; x++) {
                    Element cell = (Element) columns.item(x);

                    String value = null;
                    if(cell.hasAttribute("t")) {
                        String type = cell.getAttributeNode("t").getValue();
                        switch(type) {
                            case "s":
                                value = strings[Integer.parseInt(cell.getFirstChild().getTextContent())];
                                break;
                            case "n":
                                value = cell.getFirstChild().getTextContent();
                                break;
                            case "inlineStr":
                                Element is = (Element) cell.getFirstChild();
                                String contents = ((Element) is.getFirstChild()).getTextContent();
                                value = contents;
                                break;
                            default:
                                System.err.println("Unknown cell type " + type);
                                System.exit(1);
                        }
                    } else {
                        value = number_format.format(Double.parseDouble(cell.getFirstChild().getTextContent()));
                    }
                    row_contents += value + ",";
                }
                //Exclude the last comma and add a newline
                row_contents = row_contents.substring(0, row_contents.length() - 1) + "\n";
                csv_ostream.write(row_contents.getBytes(StandardCharsets.UTF_8));

                //Every 100 rows, switch to a new file
                if(++num_written % 100 == 0 && num_written != num_rows - 1) {
                    csv_ostream.close();
                    file_index++;
                    csv_ostream = new FileOutputStream(new File(args[1] + "/" + file_index + ".csv"));
                    csv_ostream.write(header_str.getBytes(StandardCharsets.UTF_8));
                }
            }
            csv_ostream.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
